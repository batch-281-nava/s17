//Function
/*
	Syntax:
		Function functionName(){
		code block (statement)
		};
*/
function printName(){
	console.log("My name is Marjorie");
};

printName();

// Function declaration vs expressions

function declarationFunction(){
	console.log("Hello, world from declarationFunction");
};

declarationFunction();

// Function expression
let variableFunction = function(){
	console.log("Hello again!");
};

variableFunction();

declarationFunction = function(){
	console.log("Updated declaredFunction")
};

declarationFunction();

const constantFunction = function(){
	console.log("Initialized with const!")
};

/*
constantFunc = function(){
	console.log("Cannot be re-assigned")
};

constantFunc();
*/

// Function scoping 
/*
	Scope is the accessibility (visibility of variables)

	JS Variables has 3 types of scope:
		-local/block scope
		-global scope
		-function scope
*/
{
	let localVar = "Alonzo Mattheo";
	console.log(localVar);
}

let globalVar = "Aizzac Ellis";

console.log(globalVar);

function showNames() {
	// function scoped variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}
 showNames();
 // console.log(functionVar);
 // console.log(functionConst);
 // console.log(functionLet);

 // Nested Functions

 function myNewFunction(){
 	let name = "Jane";

 	function nestedFunction(){
 		let nestedName = "John";
 		console.log(name);
 	}
 	nestedFunction();
 }

 // NestedFunction();
 myNewFunction();

 // Function and Global scoped variables
 // Global scoped variable

 let globalName = "Joy";

 function myNewFunction2(){
 	let nameInside = "Kenzo";

 	console.log(globalName);
 	console.log(nameInside)
 }

 myNewFunction2();
 // console.log(nameInside);
 // Using Alert()
  
 alert("Hello, world!"); 
 // This will run emmidiately when the page loads

 function showSampleAlert(){
 	alert("Hello, user!");
 }

 showSampleAlert();

 // Using prompt()

 let samplePrompt = prompt("Enter yor name: ");

 console.log("Hello, " + samplePrompt);

 function printWelcomeMessage(){
 	let firstName = prompt("Enter your first name: ");
 	let lastName = prompt("Enter your last name: ");

 	console.log("Hello, " + firstName + lastName + "!");
 	console.log("Welcome to my page!");
 }

 printWelcomeMessage();

 /*
 Function naming conventions
 	Function Naming conventions: Uses verb (Action words)
- Function names should be defiinitive of the the task it will perform
- Avoid generic names to avoid confusion within the code
- Avoid pointless and inappropriate function names
- Name your functions following camel casing
- Do not use JS reserved keywords
 */
