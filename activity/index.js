/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function printWelcomeMessage(){
        let firstName = prompt("Enter your first name: ");
        let lastName = prompt("Enter your last name: ");
        let yourAge = prompt("What is your age: ");
        let yourAddress = prompt("Your city of residence: ");
        console.log("Hello, " + firstName + " " + lastName);
        console.log("You are " + yourAge + " years old.");
        console.log("you live in " + yourAddress + " City");
    }

    printWelcomeMessage();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/


	//second function here:
function printFavoriteBands() {

	let band1 = "Favorite band 1"
	console.log("1. Radwimps" + band1);

	let band2 = "Favorite band 2"
	console.log("2. Yoasobi" + band2);

	let band3 = "Favorite band 3"
	console.log("3. Ben&Ben" + band3);

	let band4 = "Favorite band 4"
	console.log("4. Bread" + band4);

	let band5 = "Favorite band 5"
	console.log("5. Queen" + band5);

}
 printFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function favoriteMovies(){
	let movie1 = "1. Violet Evergarden";
	let movie2 = "2. Grave of the Fireflies";
	let movie3 = "3. Forest Gump"
	let movie4 = "4. Geostorm"
	let movie5 = "5. Titanic"

	console.log("movie1");
	console.log("Tomato meter for " + movie1 + "100%")
	console.log("movie2")
	console.log("Tomato meter for " + movie2 + "100%")
	console.log("movie3")
	console.log("Tomato meter for " + movie3 + "71%")
	console.log("movie4")
	console.log("Tomato meter for " + movie4 + "17%")
	console.log("movie5")
	console.log("Tomato meter for " + movie5 + "88%")
	
}
 favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
function printUsers(){
    let printFriends = //function printUsers(){
    alert("Hi! Please add the names of your friends.");
    let friend1 = prompt("Enter your first friend's name:"); 
    let friend2 = prompt("Enter your second friend's name:"); 
    let friend3 = prompt("Enter your third friend's name:");


    console.log("You are friends with:")
    console.log(friend1); 
    console.log(friend2); 
    console.log(friend3); 
};

printUsers();